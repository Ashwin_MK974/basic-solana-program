import React, { useState } from 'react'
import './App.css'
import * as anchor from '@project-serum/anchor'
import { Connection, PublicKey, clusterApiUrl } from '@solana/web3.js'
import idl from './idl.json'

// SystemProgram is a reference to the Solana runtime!
const { SystemProgram, Keypair } = anchor.web3

// Create a keypair for the account that will hold the variable data.
let myAccount = Keypair.generate()

// Get our program's id from the IDL file.
const programID = new PublicKey(idl.metadata.address)
console.log(programID, 'program Id set correctly')

// Set our network to devnet.
const network = 'http://127.0.0.1:8899'

// Controls how we want to acknowledge when a transaction is "done".
const opts = {
  preflightCommitment: 'processed',
}

const App = () => {
  const [walletAddress, setWalletAddress] = useState(null)

  window.onload = async function () {
    try {
      if (window.solana) {
        const solana = window.solana
        if (solana.isPhantom) {
          console.log('Phantom wallet found!')
          const res = await solana.connect({ onlyIfTrusted: true })
          console.log('Connected with Public Key:', res.publicKey.toString())
          setWalletAddress(res.publicKey.toString())
        }
      } else {
        alert('Wallet not found! Get a Phantom Wallet 👻')
      }
    } catch (error) {
      console.error(error)
    }
  }

  const connectwallet = async () => {
    if (window.solana) {
      const solana = window.solana
      const res = await solana.connect()
      setWalletAddress(res.publicKey.toString())
    } else {
      alert('Wallet not found! Get a Phantom Wallet 👻')
    }
  }
  
  const getProvider = () => 
  {
    const connection = new Connection(network, "processed")
    const provider = new anchor.AnchorProvider(connection, window.solana, "processed")
    return provider
  }

  const Retrieve = async () => {
    try {
      const provider = getProvider()
      const program = new anchor.Program(idl, programID, provider)
      const account = await program.value
      console.log(account)
      console.log(account)
    } catch (error) {
      console.log('Une erreur est survenue lors du fetch : ', error)
    }
  }

  return (
    <div>
      <p>This is new App</p>
      <button onClick={_=>{
        Retrieve();
      }}>
        Obtenir les valeurs 
      </button>
    </div>
  )
}

export default App
