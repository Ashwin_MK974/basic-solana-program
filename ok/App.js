import React, { useRef,useState } from 'react'
import './App.css'
import * as anchor from '@project-serum/anchor'
import { Connection, PublicKey, clusterApiUrl } from '@solana/web3.js'
import idl from './idl.json'
window.get=""
window.debugKeypair=""
window.target=document.querySelector('.init')
// SystemProgram is a reference to the Solana runtime!
const { SystemProgram, Keypair } = anchor.web3

// Get our program's id from the IDL file.
const programID = new PublicKey(idl.metadata.address)
console.log(programID, 'program Id set correctly')
// Set our network to devnet.
const network = 'http://127.0.0.1:8899'




const App = () => {
  const [count, setCount] = useState(null);
  const [sata,setSata] = useState(null);
  const pubKey = useRef();
  const text = useRef();
  const newData = useRef();
  const nows = useRef();


  const [walletAddress, setWalletAddress] = useState(null)
  window.onload = async function () {
    try {
      if (window.solana) {
        const solana = window.solana
        if (solana.isPhantom) {
          console.log('Phantom wallet found!')
          const res = await solana.connect()
          console.log('Connected with Public Key:', res.publicKey.toString())
          setWalletAddress(res.publicKey.toString())
        }
      } else {
        alert('Wallet not found! Get a Phantom Wallet 👻')
      }
    } catch (error) {
      console.error(error)
    }
  }




  const connection = new Connection(network, "processed")
  const getProvider = () => 
  {
    const provider = new anchor.AnchorProvider(connection, window.solana, "processed")
    return provider
  }

  const Retrieve = async () => {
    try {
      const provider = getProvider()
      const program = new anchor.Program(idl, programID, provider)
      const account = await program.account.init.all();
      window.get=account;
      setCount(account)
      console.log("Valeur de count " + count)
      console.log(account)
    } catch (error) {
      console.log('Une erreur est survenue lors du fetch : ', error)
    }
  }


  
  let user_account_generate;
  const genAccounts = ()=>{
    user_account_generate=Keypair.generate();
    setSata(user_account_generate.publicKey)
    window.debugKeypair=user_account_generate;
    console.log("La nouvelle clé générer est : " + user_account_generate.publicKey.toString())
    console.log("La nouvelle clé générer au niveau du setter est : " + sata  )
  }

  const CreateAccount = async (e) => {
    e.preventDefault();
    console.log("Le text entrer est " + text.current.value)
    try {
      console.log("Creation d'un compte utilisateur ...")
      genAccounts();
      const provider = getProvider()
      const program = new anchor.Program(idl, programID, provider)
      let tx = await program.rpc.initialize(text.current.value,{
        accounts: {
          initialAccount: user_account_generate.publicKey,
          user: provider.wallet.publicKey,
          systemProgram: SystemProgram.programId,
        },
        signers: [user_account_generate],
      })
      Retrieve(); //Pour mettre à jour automatiquement le render
    } catch (error) {
      console.log('Une erreur est survenue lors de la création du compte ... :', error)
    }
  }
  const UpdateValue = async (e) => {
    e.preventDefault();
    try {
      const provider = getProvider()
      const program = new anchor.Program(idl, programID, provider)
      console.log("La nouvelle valeur est : " , nows.current.value )
      console.log("La clé est " + sata  )
      let tx2 = await program.rpc.updateValue(nows.current.value, {
        accounts: {
          storageAccount: sata,
        },
        // signers: [account],
      })
      Retrieve() //Mise à jour du render de nouveau
    } catch (error) {
      console.log('Erreur lors de la modification', error)
    }
  }
  const getAddress = ()=>{
    console.log("La clé publique de l'utilisateur connecté est  ", walletAddress  )
  }
  if(count==null)
  {
    Retrieve();
  }
  return (
    <div className='container-main'>
      <h1>Sol Basic App</h1>
      <button>
        Obtenir la clé 
      </button>
      <div className="options">
           <form className="" onSubmit={ e => CreateAccount(e) }>
                      <input type="text" placeholder="Mot clé" required ref={text}/>
                      <input type="submit" className='tx' value="Valider"/>
          </form>
          <form className="" onSubmit={ e => {UpdateValue(e)} }>
                        {/* <input type="text" placeholder="Enter PubKey" required ref={pubKey}/> */}
                        <input type="text" placeholder="Enter new data" required ref={nows}/>
                        <input type="submit" className='tx' value="Modifier"/>
          </form> 
      </div>
      <div className="response">
      {
        //count && <p style={{marginTop:60}}>La variable est définie </p> || <p style={{marginTop:60}}>La variable n'est pas définie</p> 
        count && count.map((response)=><div className="responseFromBlockchain" key={response.publicKey.toString().slice(0,4)}>
        <p>{response.account.values}</p>
        <p>{response.account.author.toString()}</p>
        {
          walletAddress == response.account.author.toString() && 
          <div className="edit" onClick={_=>{
            console.log("Compte créer avec la clée ...", response.publicKey.toString())
            let value = response.publicKey
            setSata(value)
            console.log("Compte créer avec la clée UseState...", response.publicKey)
          }}>
          ✎
          </div>
        }
        </div>)
      }
      </div>
    </div>
  )
}

export default App
