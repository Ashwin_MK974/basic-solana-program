use {
    anchor_lang::prelude::*,
    std::convert::TryInto,
    solana_program::{
        account_info::{
            next_account_info, AccountInfo
        },
        entrypoint,
        entrypoint::ProgramResult,
        msg,
        program::invoke,
        program_error::ProgramError,
        pubkey::Pubkey,
        system_instruction,
    },
};
declare_id!("CEhPvYkBofmPm5Y1tMV2AyEJMtdYpzTod4n3wgYThicn");
#[program]
pub mod solana_crud_project {
    use super::*;

    pub fn initialize(ctx: Context<Initialize>,user_value:String) -> Result<()> {
        let author : &Signer = &ctx.accounts.user;
        let initial_account = &mut ctx.accounts.initial_account;
        initial_account.values = user_value;
        initial_account.author=*author.key;
        Ok(())
    }
    pub fn update_value(ctx: Context<UpdateValue>, value : String) -> Result<()> {
        
        let storage_account = &mut ctx.accounts.storage_account;
        storage_account.values = value;
        Ok(())

    }

    pub fn sending(ctx: Context<SolSend>) -> Result<()>
    {
        msg!("ok");
        Ok(())
    }



}
pub fn process_instruction(_program_id: &Pubkey,accounts: &[AccountInfo],input: &[u8]) -> ProgramResult {
    
    let accounts_iter = &mut accounts.iter();
    let payer = next_account_info(accounts_iter)?;
    let payee = next_account_info(accounts_iter)?;

    let amount = input
        .get(..8)
        .and_then(|slice| slice.try_into().ok())
        .map(u64::from_le_bytes)
        .ok_or(ProgramError::InvalidInstructionData)?;

    // let amount = i32::try_from_slice(input);

    msg!("Received request to transfer {:?} lamports from {:?} to {:?}.", 
        amount, payer.key, payee.key);
    msg!("  Processing transfer...");

    // Transfer from PAYER to PAYEE a specific amount:
    invoke(
        &system_instruction::transfer(payer.key, payee.key, amount),
        &[payer.clone(), payee.clone()],
    )?;
    
    msg!("Transfer completed successfully.");
    Ok(())
}
#[derive(Accounts)]
pub struct Initialize<'info>
{
    #[account(init, payer = user ,space=9000)]
    pub initial_account : Account<'info,Init>,
    #[account(mut)]
    pub user: Signer<'info>,
    pub system_program : Program<'info, System>
}

#[derive(Accounts)]
pub struct UpdateValue<'info>
{
    #[account(mut)]
    pub storage_account : Account<'info,Init>
}



#[account]
pub struct Init{
    pub values: String,
    pub author: Pubkey
}


#[derive(Accounts)]
pub struct SolSend<'info> {
    #[account(mut, signer)]
    /// CHECK: This is not dangerous because we don't read or write from this account
    pub from: AccountInfo<'info>,       
    /// CHECK: This is not dangerous because we don't read or write from this account
    #[account(mut)]
    pub to: AccountInfo<'info>,        
    /// CHECK: This is not dangerous because we don't read or write from this account
    pub system_program: AccountInfo<'info>,
}

