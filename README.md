Ce projet inclut un smart contract de Solana écrit en rust permettent de définir une structure et de la mettre à jour.
Cette structure va permettre de stocker deux informations qui concernent la valeur ainsi que la clé publique de l'auteur.
Le projet inclut également le programme d'appel en javascript qui pourra être exécuté via anchor test.

**Installation :**

Le fichier **lib.rs ** est à mettre dans le dossier **src** et le fichier **JavaScript** dans le dossier **tests** du projet Anchor
