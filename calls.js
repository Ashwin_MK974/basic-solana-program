const anchor = require('@project-serum/anchor');
const connection = require('@solana/web3.js');
const { SystemProgram } = anchor.web3;

const TestFunc = async function () {
    const provider = anchor.AnchorProvider.env();
    anchor.setProvider(provider);
    const program = anchor.workspace.SolanaCrudProject;


    const account = anchor.web3.Keypair.generate();
    const tx = await program.rpc.initialize("France", "Hello World", "google.fr", {
        accounts: {
            initialAccount: account.publicKey,
            user: provider.wallet.publicKey,
            systemProgram: SystemProgram.programId
        },
        signers: [account]
    })

    const account2 = anchor.web3.Keypair.generate();
    const tx2 = await program.rpc.initialize("Réunion", "Hello Mars", "yahoo.fr", {
        accounts: {
            initialAccount: account2.publicKey,
            user: provider.wallet.publicKey,
            systemProgram: SystemProgram.programId
        },
        signers: [account2]
    })
    console.clear()

    let fetchedAll = await program.account.init.all()

    console.log('------------------------------ | Récupération des données du premier compte | ------------------------------')
    const firstRequest = await program.account.init.fetch(account.publicKey)
    console.log(firstRequest)
    console.log('------------------------------ | Fin de Récupération des données du premier compte | ------------------------------')

    console.log(' \n \n \n')

    console.log('------------------------------ | Récupération des résultats | ------------------------------')
    console.log("Nombre d'enregistrement : " + fetchedAll.length)
    for (let a = 0; a <= fetchedAll.length - 1; a++) {
        console.log(fetchedAll[a])
    }

    console.log(' \n \n \n')

    console.log('------------------------------ | Parserialisation des résultats | ------------------------------')
    console.log(' \n \n \n')
    //Fonction qui met en style la présentation des clés sous forme de chaine de caractere ex=> hello => hELLO
    function formatPresentation(obj) {
        return obj.slice(0, 1).toUpperCase() + obj.slice(1, obj.length)
    }
    //Fonction qui va aller récupérer les données de chaque compte de donnée Solana
    function DynamicFetchResult(accountPubKey, accountIndex, modeExec) {
        console.log(`==> Récupération des données du compte ${accountIndex} <==                           `)
        const index = Object.entries(accountPubKey)
        modeExec == "--v" ? console.log("Exécution en mode verbose") : console.log("Exécution en mode non verbose")
        for (let exploreObj = 0; exploreObj <= index.length - 1; exploreObj++) {
            if (modeExec == "--v") {
                console.log("     " + formatPresentation(index[exploreObj][0].toString()) + " : " + index[exploreObj][1].toString())
            }
            else {
                console.log("     " + index[exploreObj][1].toString())
            }

            if (exploreObj == index.length - 1) {
                console.log(`| -------------------------------- Fin de récupération des données du compte ${accountIndex}  ------------------------ |`)
                console.log(' \n \n \n')
            }
        }

    }

    //Appel de la suite d'instruction qui permet de faire des modification au niveau des données d'un compte de données
    /*const value = new anchor.BN(40)
    const tx3 = await program.rpc.updateValue(value, {
        accounts: {
            storageAccount: account.publicKey
        }
    });*/

    //Boucle qui parcours le nombre de compte
    for (let a = 0; a <= fetchedAll.length - 1; a++) {
        DynamicFetchResult(await program.account.init.fetch(fetchedAll[a].publicKey), a, "--v")
    }
}

const runTest = async () => {
    try {
        await TestFunc()
        process.exit(0)
    }
    catch (error) {
        console.log(error)
        process.exit(1)
    }
}

runTest();

