use anchor_lang::prelude::*;

declare_id!("H87MqnHJcBLbjWx7FxSUnJnAUFga7PuqwHQVrqEyygRg");

#[program]
pub mod solana_crud_project {
    use super::*;

    pub fn initialize(ctx: Context<Initialize>, domain: String, text: String, service: String) -> Result<()> {
        let author : &Signer = &ctx.accounts.user;
        let initial_account = &mut ctx.accounts.initial_account;
        initial_account.value = 90;
        initial_account.address = domain;
        initial_account.author = *author.key;
        initial_account.text=text;
        initial_account.service=service;
        Ok(())
    }

    pub fn update_value(ctx: Context<UpdateValue>, value : u64) -> Result<()> {
        
        let storage_account = &mut ctx.accounts.storage_account;
        storage_account.value = value;
        Ok(())

    }
}







#[derive(Accounts)]
pub struct Initialize<'info>
{
    #[account(init, payer = user ,space=9000)]
    pub initial_account : Account<'info,Init>,
    #[account(mut)]
    pub user: Signer<'info>,
    pub system_program : Program<'info, System>
}

#[derive(Accounts)]
pub struct UpdateValue<'info>
{
    #[account(mut)]
    pub storage_account : Account<'info,Init>
}


#[account]
pub struct Init{
    pub value: u64,
    pub author: Pubkey,
    pub address:String,
    pub text: String,
    pub service:String
}
